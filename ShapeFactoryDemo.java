package factory_Pattern;

public class ShapeFactoryDemo {
	
	public static void main(String[] args) {
		
		ShapeFactory shapeFactory = new ShapeFactory();
		
		Shape circle = shapeFactory.callShape(ShapeType.CIRCLE);
		circle.draw();
		System.out.println("Square Perimeter is : " + circle.getPerimeterOfCircle(7));
		System.out.println("Square Area is : " + circle.getAreaOfCircle(7)+ "\n");
		
		Shape square = shapeFactory.callShape(ShapeType.SQUARE);
		square.draw();
		System.out.println("Square Perimeter is : " + square.getPerimeter(8,8));
		System.out.println("Square Area is : " + square.getArea(8,8)+ "\n");
		
		
		Shape rectangle = shapeFactory.callShape(ShapeType.RECTANGLE);
		rectangle.draw();
		System.out.println("Rectangle Perimeter is : " + rectangle.getPerimeter(8,6));
		System.out.println("Rectangle Area is : " + rectangle.getArea(8,6)+ "\n");
		
		
		Shape pentagon = shapeFactory.callShape(ShapeType.PENTAGON);
		pentagon.draw();
		System.out.println("Pentagon Perimeter is :" + pentagon.getPerimeterOfPentagon(10));
		System.out.println("Pentagon Area is : " + pentagon.getAreaOfPentagon(10,8));
		
		
		//
	}

}
