package factory_Pattern;

public class Square implements Shape {

	public void draw() {
		System.out.println("draw method from square");
	}

	@Override
	public double getPerimeter(double w, double h) {
		return w*4;
	}

	@Override
	public double getArea(double w, double h) {
		return w*h;
	}

	@Override
	public double getPerimeterOfPentagon(double a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getAreaOfPentagon(double a, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPerimeterOfCircle(double r) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getAreaOfCircle(double r) {
		// TODO Auto-generated method stub
		return 0;
	}

}
