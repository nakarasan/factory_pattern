package factory_Pattern;

import javax.swing.border.MatteBorder;

public class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("draw method from circle");
	}

	@Override
	public double getPerimeter(double w, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getArea(double w, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPerimeterOfPentagon(double a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getAreaOfPentagon(double a, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPerimeterOfCircle(double r) {
		return 2*Math.PI*r;
	}

	@Override
	public double getAreaOfCircle(double r) {
		return Math.PI*Math.pow(r, 2);
	}

}
