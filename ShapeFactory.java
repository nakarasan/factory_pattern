package factory_Pattern;

public class ShapeFactory {
	
	public Shape callShape(ShapeType ShapeType ) {
		
		if (ShapeType.equals(ShapeType.CIRCLE)) {
			return new Circle();
		}
		
		if (ShapeType.equals(ShapeType.SQUARE)) {
			return new Square();
		}
		
		if (ShapeType.equals(ShapeType.RECTANGLE)) {
			return new Rectangle();
		}
		
		if (ShapeType.equals(ShapeType.PENTAGON)) {
			return new Pentagon();
		}
		
		return null;
		
	}

}
