package factory_Pattern;

public interface Shape {

	public void draw();
	
	public double getPerimeter(double w, double h);
	
	public double getArea(double w, double h);
	
	public double getPerimeterOfPentagon(double a);
	
	public double getAreaOfPentagon(double a, double h);
	
	public double getPerimeterOfCircle(double r);
	
	public double getAreaOfCircle(double r);
	
}
