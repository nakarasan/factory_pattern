package factory_Pattern;

public class Pentagon implements Shape {

	public void draw() {
		System.out.println("draw method from pentagon");
	}

	@Override
	public double getPerimeter(double w, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getArea(double w, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPerimeterOfPentagon(double a) {
		// TODO Auto-generated method stub
		return 5*a;
	}

	@Override
	public double getAreaOfPentagon(double a, double h) {
		// TODO Auto-generated method stub
		return (5/2)*a*h;
	}

	@Override
	public double getPerimeterOfCircle(double r) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getAreaOfCircle(double r) {
		// TODO Auto-generated method stub
		return 0;
	}


}
